#include "sqlite3.h"
#include "DbSql.h"
#pragma comment(lib,"./song_sqlite/sqlite3.lib")

DbSql::DbSql(void)
{
	rc = 0;
	db = NULL;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
 int i;
 for (i = 0; i<argc; i++) {
  printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
 }
 printf("--callback-- argc[%x]\n",argc);
 return 0;
}

int DbSql::openDataBase(char* path)
{
	rc = sqlite3_open(path, &db);

	if (rc) {
		printf("Can't open database: %s\n", sqlite3_errmsg(db));
		return(0);
	}
	else {
		//printf("Opened database successfully\n");
	}
	return rc;
}

int DbSql::closeDB(void)
{
	//rc = sqlite3_exec (db, "END TRANSACTION;", NULL, NULL, NULL);
	sqlite3_close(db);
	return 1;
}

int DbSql::createDB(void)
{
	rc=0;
	const char *sql;
sql = "CREATE TABLE IF NOT EXISTS haMIDI("  \
	"iSongNo INT PRIMARY KEY NOT NULL,"\
	"iGenre         INT,"\
	"sTitle         CHAR(128),"\
	"sSubTitle      CHAR(128),"\
	"sKeyCode       CHAR(128),"\
	"sComposer      CHAR(128),"\
	"sWriter        CHAR(128),"\
	"sSinger        CHAR(128),"\
	"iKeyCodeN      INT,"\
	"iDate			INT,"\
	"iCountry       INT,"\
	"iCodePage      INT,"\
	"iVocal			INT,"\
	"iChrous        INT,"\
	"iMRType        INT,"\
	"iMusicType 	INT,"\
	"iYearType 	    INT,"\
	"sReadTitle     CHAR(128),"\
	"sReadSinger    CHAR(128),"\
	"sVideoTempo    CHAR(128),"\
	"sSongAutho     CHAR(128),"\
	"sReserve1      CHAR(128),"\
	"sReserve2      CHAR(128));";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	//printf("sql[%d] -> %s\n",rc, sql);
	return rc;
}

int DbSql::insertDB(SONG_COLUMN sc)
{
	char query[2048];
	const char *insertsql;
	const char *updatesql;
///////------------------------
insertsql = "INSERT INTO haMIDI (\
iSongNo, \
iGenre, \
sTitle,\
sSubTitle,\
sKeyCode,\
sComposer,\
sWriter,\
sSinger,\
iKeyCodeN,\
iDate,\
iCountry,\
iCodePage,\
iVocal,\
iChrous,\
iMRType,\
iMusicType,\
iYearType) \
VALUES (%d, %d, '%s','%s','%s','%s','%s','%s',%d, %d, %d, %d, %d, %d, %d, %d, %d); ";

//UPDATE table_name
//SET column1 = value1, column2 = value2....columnN=valueN
//[ WHERE  CONDITION ];
updatesql = "UPDATE haMIDI SET \
iGenre=%d, \
sTitle='%s',\
sSubTitle='%s',\
sKeyCode='%s',\
sComposer='%s',\
sWriter='%s',\
sSinger='%s',\
iKeyCodeN=%d,\
iDate=%d,\
iCountry=%d,\
iCodePage=%d,\
iVocal=%d,\
iChrous=%d,\
iMRType=%d,\
iMusicType=%d,\
iYearType=%d \
WHERE iSongNo=%d;";


sprintf_s(query, insertsql,
	sc.iSongNo, 
	sc.iGenre,//2
	sc.sTitle.c_str(),
	sc.sSubTitle.c_str(),
	sc.sKeyCode.c_str(),
	sc.sComposer.c_str(),
	sc.sWriter.c_str(),
	sc.sSinger.c_str(),//6
	sc.iKeyCodeN,
	sc.iDate,
	sc.iCountry,
	sc.iCodePage,
	sc.iVocal,
	sc.iChrous,
	sc.iMRType,
	sc.iMusicType,
	sc.iYearType//9
	);
  /* Execute SQL statement */
printf("query ->\n %s\n", insertsql);
	rc = sqlite3_exec(db, query, callback, 0, &zErrMsg);

	if( rc != SQLITE_OK ) 
	{
		//fprintf(stderr, "##INSERT error: %s\n", zErrMsg);
		sprintf_s(query, updatesql,
			sc.iGenre,//2
			sc.sTitle.c_str(),
			sc.sSubTitle.c_str(),
			sc.sKeyCode.c_str(),
			sc.sComposer.c_str(),
			sc.sWriter.c_str(),
			sc.sSinger.c_str(),//6
			sc.iKeyCodeN,
			sc.iDate,
			sc.iCountry,
			sc.iCodePage,
			sc.iVocal,
			sc.iChrous,
			sc.iMRType,
			sc.iMusicType,
			sc.iYearType,//9
			sc.iSongNo 
			);

		printf("query ->\n %s\n", query);
		rc = sqlite3_exec(db, query, callback, 0, &zErrMsg);
		if( rc != SQLITE_OK ) fprintf(stderr, "##UPDATE error: %s\n", zErrMsg);
		//else  fprintf(stdout, "##UPDATE successfully\n");
	} 
	//else fprintf(stdout, "##INSERT done successfully\n");
	return rc;
}


void DbSql::delete_table() {
	const char* query;
	query = "DELETE FROM haMIDI";
	rc = sqlite3_exec(db, query, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) printf("##DELETE error: %s\n", zErrMsg);
	else  printf("=> DELETE  successfully\n");

}


