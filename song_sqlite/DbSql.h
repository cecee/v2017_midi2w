#pragma once
#include <direct.h>
#include <stdlib.h>
#include <stdio.h>
#include <io.h>       //access
#include <iostream>
#include <string.h>
#include "sqlite3.h"

using namespace std;

typedef struct _SONG_COLUMN
{
	int iSongNo;
	int iGenre;
	string sTitle;
	string sSubTitle;
	string sKeyCode;
	string sComposer;
	string sWriter;
	string sSinger;
	int iKeyCodeN;
	int iDate;
	int iCountry;
	int iCodePage;
	int iVocal;
	int iChrous;
	int iMRType;
	int iMusicType;
	int iYearType;
	string sReadTitle;
	string sReadSinger;
	string sVideoTempo;
	string sSongAutho;
	string sReserve1;
	string sReserve2;
}SONG_COLUMN;


class DbSql
{
public:
	int rc;
	sqlite3 *db;
	char *zErrMsg;
	SONG_COLUMN song_column;
	DbSql(void);
	int openDataBase(char* path);
	int createDB(void);
	int insertDB(SONG_COLUMN sc);
	int closeDB(void);
	void delete_table(void);
};
