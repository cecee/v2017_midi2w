﻿// v2017_midi2w.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//
#define _AFXDLL    //Console application에서 MFC 관련 함수
#include "Afx.h"
#include <iostream>
#include "./lyric_util/LyricUtil.h"
#include "./song_sqlite/DbSql.h"
#include "./midi_util/MIDI_Util.h"

using namespace std;

DbSql dbsql;
LyricUtil lyric_util;
MIDI_Util midi_util;


// Number of strings in array argv
// Array of command-line argument strings
// Array of environment variable strings
int main(int argc, char* argv[], char* envp[])  
{
	int i, ix, count;
	int rc, rtn;
	bool is;
	string str, dst_file, xstr, db_file;
	SONG_COLUMN sColume;
	char temp_buf[256] = { 0, };
	char dst_buf[256] = { 0, };

#ifdef TXT_DBG
	system("chcp 65001");//콘솔 UTF-8이용
#endif

#if 0 
	// Display each command-line argument.
	cout << "\nCommand-line arguments:\n";
	for (count = 0; count < argc; count++)
		cout << "  argv[" << count << "]   " << argv[count] << "\n";
#endif
	//printf("%s\n", argv[1]);
	str = argv[1];
	dst_file = argv[2];
	db_file = "upSqlite.db";
	memset(temp_buf, 0, sizeof(temp_buf));
	sprintf(temp_buf, "%s", db_file.c_str());
	sprintf(dst_buf, "%s", dst_file.c_str());

	rc = dbsql.openDataBase(temp_buf);
	rc = dbsql.createDB(); 
	//delete all table
	//dbsql.delete_table();
	
	xstr = lyric_util.splitFile(str);
	xstr = lyric_util.ReplaceAll(xstr, ".txt", "");
	is = lyric_util.isNumber(xstr);

#if 1
	if (is) {
		memset(temp_buf, 0, sizeof(temp_buf));
		sprintf(temp_buf, "%s", str.c_str());
		sColume = lyric_util.makeSongColumnXml(temp_buf);
		rtn = midi_util.makeHaMidiW(temp_buf, dst_buf);
		if (rtn) {
			rc = dbsql.insertDB(sColume);
		}
		//printf("-->insertDB rc[%d] \n", rc);
	}
	//else continue;
//}
#endif
	dbsql.closeDB();
}

