#pragma once
#define xDBG
#define xNOTE_DBG
#define TXT_DBG

#pragma warning(disable : 4996)
#define CP_THAI 874
#define CP_SJIS 932
#define CP_GBK 936
#define CP_KOREA 949
#define CP_BIG5 950
#include <string>
#include <vector>
#include <string.h>
#include <stdio.h>
#include "../song_sqlite/DbSql.h"


using namespace std;

typedef unsigned short WORD;
typedef unsigned long DWORD; 

#define TRIM_SPACE " \t\n\v"
/// <summary>
inline string trim(std::string& s, const std::string& drop = TRIM_SPACE)
{
	std::string r = s.erase(s.find_last_not_of(drop) + 1);
	return r.erase(0, r.find_first_not_of(drop));
}

class LyricUtil
{
public:

	vector<std::string> vGasaBlock;
	//string trim(std::string& s, const std::string& drop);
	vector<string> split(string data, string token);
	std::string splitFile(const std::string& str);
	std::string splitPath(const std::string& str);
	bool isNumber(const string& str);
	//SONG_COLUMN makeSongColumn(char* file);
	SONG_COLUMN makeSongColumnXml(const char* xmlfile);
	vector<string> ParseLyricFromTXT(const char* txtFile);//chang-myung midi
	wchar_t* getUNIBufFromTXT(const char* txtFile);//chang-myung midi
	std::string ReplaceAll(std::string &str, const std::string& from, const std::string& to);
	wchar_t* utf2uni(const char* strUTF8);
	char* uni2utf(const wchar_t* uniStr);
	char* uni2CP_ACP(wchar_t* strUNICODE);
	string strConv_2Utf(string strIn, int char_code);
	//vector<string> cmMidiLyricText(const char* txtFile);
	LyricUtil(void);
	~LyricUtil(void);
};
