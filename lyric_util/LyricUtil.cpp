//#include "StdAfx.h"//Console application에서 MFC 관련 함수
#define _AFXDLL    //Console application에서 MFC 관련 함수
#include "Afx.h"
#include <string> //stoi
#include <vector>
#include <string.h>
#include <stdio.h>
#include <vector>
#include "LyricUtil.h"
#include "tinyxml2.h"


using namespace tinyxml2;
using namespace std;



LyricUtil::LyricUtil(void)
{
	vGasaBlock.clear();
}

LyricUtil::~LyricUtil(void)
{
}

vector<string> LyricUtil::split(string data, string token)
{
    vector<string> output;
    size_t pos = string::npos; // size_t to avoid improbable overflow
    do
    {
        pos = data.find(token);
        output.push_back(data.substr(0, pos));
        if (string::npos != pos)
            data = data.substr(pos + token.size());
    } while (string::npos != pos);
    return output;
}

bool LyricUtil::isNumber(const string& str)
{
 	return str.find_first_not_of("0123456789") == string::npos;
}

std::string LyricUtil::splitPath(const std::string& str) {

	unsigned found = str.find_last_of("/\\");
	return str.substr(0, found);
}

std::string LyricUtil::splitFile(const std::string& str) {
	unsigned found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

std::string LyricUtil::ReplaceAll(std::string &str, const std::string& from, const std::string& to)
{
    size_t start_pos = 0; //string처음부터 검사
    while((start_pos = str.find(from, start_pos)) != std::string::npos)  //from을 찾을 수 없을 때까지
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // 중복검사를 피하고 from.length() > to.length()인 경우를 위해서
    }
    return str;
}


wchar_t* LyricUtil::utf2uni(const char* strUTF8)
{
	int nLen;
	wchar_t strUnicode[80000] = {0,};
	//wmemset(strUnicode,0,sizeof(strUnicode));
	nLen = MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), NULL, NULL);
	MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), strUnicode, nLen);
	return strUnicode;
}


char* LyricUtil::uni2utf(const wchar_t* uniStr)
{
	char strUtf8[80000] = { 0, }; 
	int nLen = WideCharToMultiByte(CP_UTF8, 0, uniStr, lstrlenW(uniStr), NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_UTF8, 0, uniStr, lstrlenW(uniStr), strUtf8, nLen, NULL, NULL);
	return strUtf8;
}



char* LyricUtil::uni2CP_ACP(wchar_t* strUNICODE)
{
	int len;
	char strCP_ACP[256]; 
	memset(&strCP_ACP,0,256);
	len = WideCharToMultiByte( CP_ACP, 0, strUNICODE, -1, NULL, 0, NULL, NULL );
	WideCharToMultiByte( CP_ACP, 0, strUNICODE, -1, strCP_ACP, len, NULL, NULL );
	return strCP_ACP;
}

string LyricUtil::strConv_2Utf(string strIn, int char_code)
{
	string strOut;
	wchar_t strUni[512];
	char strUtf8[512] ={0,};
	char pData[512];
	int nLen;
	strcpy(pData, strIn.c_str());
	memset(strUni,0,sizeof(strUni));
	memset(strUtf8,0,sizeof(strUtf8));
	MultiByteToWideChar( char_code, 0, pData, -1, strUni, 256 );
	nLen = WideCharToMultiByte(CP_UTF8, 0, strUni, lstrlenW(strUni), NULL, 0, NULL, NULL);
	WideCharToMultiByte (CP_UTF8, 0, strUni, lstrlenW(strUni), strUtf8, nLen, NULL, NULL);
	strOut=(char*)strUtf8;
	return strOut;
}

vector<string> LyricUtil::ParseLyricFromTXT(const char* txtFile)//chang-myung midi
{
	vector<wchar_t> vUniText;

	FILE *pFile;
	char temp_buf[256]={0,};
	char utf_buf[80000]={0,};
	wchar_t uni_tmp[80000] = { 0, };
	string str,xstr,ystr;
	int lyricStart=0;
	wchar_t* uni_buf;
	wchar_t uniChar;
	wchar_t wtemp[256];
	char ltemp[512];
	vector<string> v;
	vector<string> w;
	int i,ix;
	w.clear();
#ifdef TXT_DBG
	printf("####cmMidiLyricText [%s]\n", txtFile);
#endif
	pFile=fopen(txtFile, "rb"); 
	if(pFile == NULL){
		printf("fopen Error\n");
		return w;
	}
	str="";
	fgetc(pFile);//skip BOM
	fgetc(pFile);//skip BOM
	for (;;) {
		fgetws(wtemp, 256, pFile);
		memcpy(ltemp, uni2utf(wtemp), 256);
		xstr = ltemp;
		if (xstr.find("<LYRIC MAIN>") != string::npos) {
			fgetws(wtemp, 256, pFile);//<CLASS = KORKORCC> skip
			break;
		}
	}

	while (fgetws(wtemp, 256, pFile) != NULL) {
		memcpy(ltemp, uni2utf(wtemp), 256);
		xstr = ltemp;
		if (xstr.find("</LYRIC MAIN>") != string::npos) {
			break;
		}
		str = str + xstr;
	}

	//printf("####str [%s]\n", str.c_str());
	////전체 unicode로 만들기
	ReplaceAll(str, "\r", "@");//¶
	ReplaceAll(str, "\n", "@");
	ReplaceAll(str, "@@", "@");
	ReplaceAll(str, " ", "_");

	sprintf(utf_buf, "%s", str.c_str());
	uni_buf = utf2uni(utf_buf);

	ix = 0;
	for (i = 0; i < 40000; i++) {
		uniChar = uni_buf[i];
		if (uniChar == 0) break;
		if (uniChar > 0x80) {
			uni_tmp[ix++] = '|';
			uni_tmp[ix++] = uniChar;
			
		}
		else if (uniChar == '_' || uniChar == '@') //space, enter
		{
			uni_tmp[ix++] = '|';
			uni_tmp[ix++] = uniChar;
		}
		else {
			uni_tmp[ix++] = uniChar;
		}
	}
	uni_tmp[ix++] = '^';

	memcpy(utf_buf, uni2utf(uni_tmp), 40000);
	str = utf_buf;
	ReplaceAll(str, "|@", "@|");
	ReplaceAll(str, "|_", "_|");
	ReplaceAll(str, "(|", "(");

	ReplaceAll(str, "||", "|");
	//ending처리
	ReplaceAll(str, "|^", "");
	ReplaceAll(str, "^", "");
	v = split(str, "|");
	w.clear();
	for (i = 0; i < v.size(); i++) {
		xstr = v[i];
		if (xstr.find("(") != string::npos) {
			ix = 0;
			ystr = xstr;
			while (1) {
				i++;
				xstr = v[i];
				ystr = ystr + xstr;
				if (xstr.find(")") != string::npos) break;
			}
		}
		else ystr = xstr;
		w.push_back(ystr);
	}

	printf("%s\n ->%d\n", str.c_str(), v.size());

	//for (i = 0; i < w.size(); i++) {
	//	printf("gasa[%d][%s]\n",i,  w[i].c_str());
	//	//vGasaBlock.push_back(v[i]);
	//}
	return w;
	
}

wchar_t* LyricUtil::getUNIBufFromTXT(const char* txtFile)//chang-myung midi
{
	vector<wchar_t> vUniText;

	FILE* pFile;
	char temp_buf[256] = { 0, };
	char utf_buf[80000] = { 0, };
	//wchar_t uni_tmp[80000] = { 0, };
	//wchar_t uniBuf[80000] = { 0, };
	string str, xstr, ystr;
	int lyricStart = 0;
	wchar_t* uni_buf=NULL;
	wchar_t uniChar;
	wchar_t wtemp[256];
	char ltemp[512];
	vector<string> v;
	vector<string> w;
	int i, ix;
	w.clear();
#ifdef TXT_DBG
	printf("####cmMidiLyricText [%s]\n", txtFile);
#endif
	pFile = fopen(txtFile, "rb");
	if (pFile == NULL) {
		printf("fopen Error\n");
		return uni_buf;
	}
	str = "";
	fgetc(pFile);//skip BOM
	fgetc(pFile);//skip BOM
	for (;;) {
		fgetws(wtemp, 256, pFile);
		memcpy(ltemp, uni2utf(wtemp), 256);
		xstr = ltemp;
		if (xstr.find("<LYRIC MAIN>") != string::npos) {
			fgetws(wtemp, 256, pFile);//<CLASS = KORKORCC> skip
			break;
		}
	}

	while (fgetws(wtemp, 256, pFile) != NULL) {
		memcpy(ltemp, uni2utf(wtemp), 256);
		xstr = ltemp;
		if (xstr.find("</LYRIC MAIN>") != string::npos) {
			break;
		}
		str = str + xstr;
	}

	//printf("####str [%s]\n", str.c_str());
	////전체 unicode로 만들기
	ReplaceAll(str, "\r", "@");//¶
	ReplaceAll(str, "\n", "@");
	ReplaceAll(str, "@@", "@");
	ReplaceAll(str, " ", "_");
	ReplaceAll(str, "__", "_");
	sprintf(utf_buf, "%s", str.c_str());
	uni_buf = utf2uni(utf_buf);
	return uni_buf;
#if 0
	ix = 0;
	for (i = 0; i < 40000; i++) {
		uniChar = uni_buf[i];
		if (uniChar == 0) break;
		if (uniChar > 0x80) {
			uni_tmp[ix++] = '|';
			uni_tmp[ix++] = uniChar;

		}
		else if (uniChar == '_' || uniChar == '@') //space, enter
		{
			uni_tmp[ix++] = '|';
			uni_tmp[ix++] = uniChar;
		}
		else {
			uni_tmp[ix++] = uniChar;
		}
	}
	uni_tmp[ix++] = '^';

	memcpy(utf_buf, uni2utf(uni_tmp), 40000);
	str = utf_buf;
	ReplaceAll(str, "|@", "@|");
	ReplaceAll(str, "|_", "_|");
	ReplaceAll(str, "(|", "(");

	ReplaceAll(str, "||", "|");
	//ending처리
	ReplaceAll(str, "|^", "");
	ReplaceAll(str, "^", "");
	v = split(str, "|");
	w.clear();
	for (i = 0; i < v.size(); i++) {
		xstr = v[i];
		if (xstr.find("(") != string::npos) {
			ix = 0;
			ystr = xstr;
			while (1) {
				i++;
				xstr = v[i];
				ystr = ystr + xstr;
				if (xstr.find(")") != string::npos) break;
			}
		}
		else ystr = xstr;
		w.push_back(ystr);
	}

	printf("%s\n ->%d\n", str.c_str(), v.size());

	//for (i = 0; i < w.size(); i++) {
	//	printf("gasa[%d][%s]\n",i,  w[i].c_str());
	//	//vGasaBlock.push_back(v[i]);
	//}
#endif
	//return uniBuf;

}

const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
	return buf;
}

SONG_COLUMN LyricUtil::makeSongColumnXml(const char* xmlfile) {
	FILE* pFile;
	SONG_COLUMN sColume;
	char ltemp[256] = { 0, };
	wchar_t wtemp[256] = { 0, };
	string xmlStr, str, xstr, ystr, sex;
	vector<string> vStr;
	int i, ix, x;
	char _xml[40000];
	const char* value;
	XMLElement* element;
	unsigned char err;

	tinyxml2::XMLDocument doc;

	pFile = fopen(xmlfile, "rb");
	if (pFile == NULL) {
		printf("fopen Error\n");
		return sColume;
	}
	//SongNumber----------------------------
	str = xmlfile;
	//printf("SongNumber:%s\n", str.c_str());
	xstr = splitFile(str);
	x = atoi(xstr.c_str());
	sColume.iSongNo = x;

	//Date------------------------------
	str = currentDateTime();
	x = atoi(str.c_str());
	sColume.iDate = x;
	printf("[c++]iSongNo[%d] Date[%d]\n", sColume.iSongNo, sColume.iDate);

	/*------------------------------------------------
	* unicode로 작성된 xml file을 utf-8로 바꿔서 사용
	* xml parse
	-------------------------------------------------*/
	fgetc(pFile);//skip BOM
	fgetc(pFile);//skip BOM
	xmlStr = "";
	while (fgetws(wtemp, 256, pFile) != NULL) {
		memcpy(ltemp, uni2utf(wtemp), 256);
		xstr = ltemp;
		if (xstr.find("<VERSION") != string::npos) { ; }
		else if (xstr.find("KORKORCC") != string::npos) { ; }
		else if (xstr.find("ENGUSACC") != string::npos) { ; }//2021.06.01 영어노래 대응
		else if (xstr.find("LYRIC MAIN") != string::npos) { 
			xstr = ReplaceAll(xstr, "LYRIC MAIN", "LYRICMAIN");
			xmlStr = xmlStr + xstr;
		}
		else xmlStr = xmlStr + xstr;
	}
	fclose(pFile);

	printf("[c++]prepare xml\n");
	sprintf((char *)_xml, "%s", xmlStr.c_str());
	err=doc.Parse(_xml);
	if(err) printf("XML Parse Error_ID[%d]\r\n ", err);
	
	auto* pRoot = doc.RootElement();
	if (pRoot == nullptr) {
		printf("XML Error  pRoot nulptr\n");
		return sColume;
	}
	////////////////////////////////////////////////
	printf("[c++]prepare xml--ok\n");

	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("TITLE");
	//printf("titleElement[%x]\n", titleElement);
	value = (char *)element->GetText();
	xstr = value;
	xstr = trim(xstr);
	xstr = ReplaceAll(xstr, "\r", "");
	xstr = ReplaceAll(xstr, "'", "''");
	sColume.sTitle = xstr;

	////////////////////////////////////////////////
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("SINGER");
	value = (char*)element->GetText();
	xstr = value;
	xstr = trim(xstr);
	xstr = ReplaceAll(xstr, "\r\n", "@");
	xstr = ReplaceAll(xstr, "\n", "@");
	vector<string> vSplit = split(xstr, "@");

	if (vSplit.size() > 1) {
		xstr = vSplit[0];
		for (ix = 1; ix < vSplit.size(); ix++) {
			xstr = xstr + "(" + vSplit[ix] + ")";
		}
	}
	else xstr = vSplit[0];
	xstr = ReplaceAll(xstr, "'", "''");
	sColume.sSinger = xstr;

	////////////////////////////////////////////////
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("COMPOSER");
	value = (char*)element->GetText();
	if (value == NULL) xstr = "";
	else xstr = value;
	xstr = trim(xstr);
	xstr = ReplaceAll(xstr, "'", "''");
	sColume.sComposer = xstr;

	////////////////////////////////////////////////
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("WRITER");
	value = (char*)element->GetText();
	if (value == NULL) xstr = "";
	else xstr = value;
	xstr = trim(xstr);
	xstr = ReplaceAll(xstr, "'", "''");
	sColume.sWriter = xstr;

	////////////////////////////////////////////////
#if 0
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("COUNTRY");
	value = (char*)element->GetText();
	if (value == NULL) xstr = "KOR";
	else xstr = value;
	xstr = trim(xstr);
	if (xstr.find("KOR") != string::npos) {
		sColume.iCountry = 410;
	}
	else sColume.iCountry = 123;
	//sColume.sWriter = xstr;
#endif
	sColume.iCountry = 410;
	////////////////////////////////////////////////
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("GENDER");
	value = (char*)element->GetText();
	if (value == NULL) xstr = "여";
	else xstr = value;

	xstr = trim(xstr);
	if (xstr.find("Male") != string::npos) {
		sex = "남";
	}
	else if (xstr.find("Female") != string::npos) {
		sex = "여";
	}
	else sex = "혼성";

	sex = strConv_2Utf(sex, CP_KOREA);//한글을 UTF-8 string으로 바꿈
	sColume.iGenre = 33;

	////////////////////////////////////////////////
	element = doc.FirstChildElement("HDT")->FirstChildElement("HEAD")->FirstChildElement("KEY");
	value = (char*)element->GetText();
	if (value == NULL) xstr = "C";
	else xstr = value;
	xstr = trim(xstr);
	sprintf(ltemp, "%s(%s)\0", sex.c_str(), xstr.c_str());
	str = ltemp;
	//ystr = strConv_2Utf(str, CP_KOREA);
	sColume.sKeyCode = str;

	//코드페이지--------------------------------
	sColume.iCodePage = 1200;//16bit unicode //949 ...
//기타
	sColume.iVocal = 0;
	sColume.iChrous = 0;
	sColume.iMRType = 0;
	sColume.iMusicType = 0;
	sColume.iYearType = 0;
	sColume.iKeyCodeN = 88;
#ifdef DBG
	printf("============================================\n");
	printf("iSongNo:%d\n", sColume.iSongNo);
	printf("iGenre:%d \n", sColume.iGenre);
	printf("sTitle:%s \n", sColume.sTitle.c_str());
	printf("sKeyCode:%s \n", sColume.sKeyCode.c_str());
	printf("sComposer:%s \n", sColume.sComposer.c_str());
	printf("sWriter:%s \n", sColume.sWriter.c_str());
	printf("sSinger:%s \n", sColume.sSinger.c_str());
	printf("iKeyCodeN:%d \n", sColume.iKeyCodeN);
	printf("iDate:%d \n", sColume.iDate);
	printf("iCountry:%d \n", sColume.iCountry);
	printf("iCodePage:%d \n", sColume.iCodePage);
	printf("==============================================\n");
#endif

	return sColume;
}


