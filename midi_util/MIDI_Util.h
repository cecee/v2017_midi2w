#pragma once

//#define CP_WANSUNG 949
#define CP_THAI 874
#define CP_SJIS 932
#define CP_GBK 936
#define CP_KOREA 949
#define CP_BIG5 950

#include "MIDIData.h"
#include <string>
#include <vector>
#include <string.h>
#include <stdio.h>
#include "../song_sqlite/DbSql.h"

using namespace std;

typedef unsigned short WORD;
typedef unsigned long DWORD;


/// </summary>
typedef struct _WFILE_STRUCTURE//
{
	WORD code_id;//2byte
	WORD count;//2byte
}WFILE_STRUCTURE;


typedef struct _LYRIC_EVENT//
{
	long tick;
	long ms; 
	long duration;
	string utf8_str;
}LYRIC_EVENT;

typedef struct _NOTE_EVENT//
{
	long ch;
	long tick;
	DWORD ms;
	long duration;
}NOTE_EVENT;

typedef struct _HA_LYRIC//
{
	wchar_t uniStr;//unicode
	DWORD ms; //ms
}HA_LYRIC;

typedef struct _LYRIC_RESULT//
{
	long LyricCnt;
	long NoteCnt;
	long isTempoChange;
	long isTempo;
	long isOK;
	long codeType;
}LYRIC_RESULT;

typedef struct _TITLE//
{
	long pos;
	string str;
}TITLE;

typedef struct _SONG_STRUCTURE
{
	int iSongNo;
	int iGenre;
	string sTitle;
	string sSubTitle;
	string sKeyCode;
	string sComposer;
	string sWriter;
	string sSinger;
	int iKeyCodeN;
	int iDate;
	int iContry;
	int iCodePage;
	int iVocal;
	int iChrous;
	int iMRType;
	int iMusicType;
	int iYearType;
	string midiFileName;
	string txtFileName;
}SONG_STRUCTURE;

class MIDI_Util
{
public:
	MIDI_Util(void);
	~MIDI_Util(void);
	vector<LYRIC_EVENT> vLyricEvent;
	SONG_STRUCTURE song;
	string source_path;
	string target_path;
	string current_path;
	DWORD MIDILyric_StartTime_ms;
	DWORD MIDILyric_EndTime_ms;
	int makeHaMidiW(char* txtfile, char* dst_file);
	vector<LYRIC_EVENT> HaMidiLyricEvent(char* midiFile);
	vector<NOTE_EVENT> HaMidiNoteEvent(MIDIData* pMIDIData, MIDITrack* pMIDITrack);
	vector<HA_LYRIC> combineEvtText(vector<DWORD> vTime, vector<wchar_t> vUniText);
	void makeHaWFILE(char* ofile, vector<HA_LYRIC> vHaLyric);
	vector<HA_LYRIC> putDummy(vector<HA_LYRIC> vLET);
	string getCurrentPath(void);

};
