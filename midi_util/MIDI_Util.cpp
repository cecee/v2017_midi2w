
//#include "StdAfx.h"//Console application에서 MFC 관련 함수
#define _AFXDLL    //Console application에서 MFC 관련 함수
#pragma comment(lib,"./midi_util/MIDIData.lib")

#include "Afx.h"

#include <afxwin.h>
#include <afxext.h>
#include <afxcmn.h>
#include <afxmt.h>

#include <fstream>//
#include <io.h>  
#include <stdlib.h>  
#include <stdio.h>
#include <cstdio>
#include <vector>
#include <iostream>//
#include <string> //stoi
#include <time.h>
#include "MIDIData.h"
#include "MIDI_Util.h"
#include "../lyric_util/LyricUtil.h"
 
      
#define TRIM_SPACE " \t\n\v"
using namespace std;

extern LyricUtil lyric_util;

SONG_STRUCTURE cbSong;

MIDI_Util::MIDI_Util(void)
{
	vLyricEvent.clear();
	DWORD MIDILyric_StartTime_ms=0;
	DWORD MIDILyric_EndTime_ms=0;
}

MIDI_Util::~MIDI_Util(void)
{
	//closeSqlite();
}

char* _trim(char* buf, int size)
{
  char *temp;// = new char[size];
  temp=(char*)malloc(size+1);
  memset(temp, 0, size+1);
  int j = 0;
  for(int i=0; i<size; i++)
  {
    if(buf[i] != ' ') temp[j++] = buf[i];
    if(buf[i]==0) break;
  }
  strcpy(buf, temp);
  free(temp);
  return buf;
}

 string rtrim(string s, const std::string& drop = TRIM_SPACE)
 {
  return s.erase(s.find_last_not_of(drop)+1);
 }

//특정문자 count
int match_count(char *c, char x)
{
	int i, count;
	count=0;
	for(i=0;c[i]!=NULL;i++)
	{
		if(c[i]==x)
			count++;
		else
			continue;
	}
return count;
}


string MIDI_Util::getCurrentPath(void){

    char* cwd = _getcwd( 0, 0 ) ; // **** microsoft specific ****
    std::string working_directory(cwd) ;
    std::free(cwd) ;
    return working_directory ;
}

vector<NOTE_EVENT> MIDI_Util::HaMidiNoteEvent(MIDIData* pMIDIData, MIDITrack* pMIDITrack)
{
		MIDIEvent* pMIDIEvent = NULL;
		vector< NOTE_EVENT> vNoteEvent;
		NOTE_EVENT m_noteEvent;
		long channel;
		long ltime_tick;
		DWORD ltime_ms;
		long ltime_duration;
		vNoteEvent.clear();
		forEachEvent(pMIDITrack, pMIDIEvent) {
			//printf("pMIDITrack->%p\n", pMIDITrack);
			if (MIDIEvent_IsNoteOn(pMIDIEvent)) {
				channel = MIDIEvent_GetChannel(pMIDIEvent);
				ltime_tick = MIDIEvent_GetTime(pMIDIEvent);
				ltime_duration = MIDIEvent_GetDuration(pMIDIEvent);
				ltime_ms = MIDIData_TimeToMillisec(pMIDIData, ltime_tick);
				m_noteEvent.ch = channel;
				m_noteEvent.tick = ltime_tick;
				m_noteEvent.ms = ltime_ms;
				m_noteEvent.duration = ltime_duration;
				vNoteEvent.push_back(m_noteEvent);
			}
		}
		return vNoteEvent;
}

vector<LYRIC_EVENT> MIDI_Util::HaMidiLyricEvent(char* midiFile){
	MIDIData *pMIDIData=NULL;
	MIDITrack *pMIDITrack=NULL;
	MIDITrack *pNextTrack=NULL;
	MIDIEvent *pMIDIEvent=NULL;
	MIDITrack *pLyricTrack=NULL;
	vector< NOTE_EVENT> vNoteEvent;
	vector<string>vTextEvent;

	char szBuf[1024];
	char pData[128];
	long ltime;
	DWORD ltime_ms;
	string track_name;
	string str,xstr,ystr;

	vector<DWORD> vTime;
	vector<LYRIC_EVENT> _vLyricEvent;
	LYRIC_EVENT _lyricEvent;
	long trackEndTime_tick;
	long trackEndTime_ms;
	int i;
	_vLyricEvent.clear();
	
	//printf("HaMidiLyricEvent:%s\n",midiFile);
	pMIDIData = MIDIData_LoadFromSMFA (midiFile);
	if (pMIDIData == NULL) {
		printf ( "Can not open MIDI file!!!\n" );
		vTime.clear();
		return _vLyricEvent;
	}
	//printf("ok pMIDIData[%p]->%s\n", pMIDIData, midiFile);
	pMIDITrack=pMIDIData->m_pFirstTrack;
	while(pMIDITrack) {
		//printf("pMIDITrack->%p\n", pMIDITrack);
		MIDITrack_GetNameA(pMIDITrack,szBuf, sizeof(szBuf));
		track_name = szBuf;
		if (track_name.find("Lyric") != string::npos) {
			trackEndTime_tick = MIDITrack_GetEndTime(pMIDITrack);
			MIDILyric_EndTime_ms = MIDIData_TimeToMillisec(pMIDIData, trackEndTime_tick);
			pLyricTrack = pMIDITrack;
			break;
		}
		pNextTrack=pMIDITrack->m_pNextTrack;
		pMIDITrack=pNextTrack;
	}

	forEachEvent(pLyricTrack, pMIDIEvent) {
		if (MIDIEvent_IsNoteOn(pMIDIEvent)) {
			long channel = MIDIEvent_GetChannel(pMIDIEvent);
			if (channel == 15) {
				 long t= MIDIEvent_GetTime(pMIDIEvent);
				 MIDILyric_StartTime_ms = MIDIData_TimeToMillisec(pMIDIData, t);
				break;
			}
		}
	}
	//get note event
	//vNoteEvent = HaMidiNoteEvent(pMIDIData, pLyricTrack);
	//vTextEvent = lyric_util.ParseLyricFromTXT(txtFile);


#ifdef NOTE_DBG
	printf("vNoteEvent count [%d] \n", vNoteEvent.size());
	for (i = 0; i < vNoteEvent.size(); i++) {
		printf("vNoteEvent[%04d] ch[%04d] ltime[%04d]\n", i, vNoteEvent[i].ch, vNoteEvent[i].tick);
	}
#endif
	forEachEvent (pLyricTrack, pMIDIEvent) {
		//long channel= MIDIEvent_GetChannel(pMIDIEvent);
		if(MIDIEvent_IsLyric(pMIDIEvent))
		{
			MIDIEvent_GetTextA(pMIDIEvent, pData,128);
			ltime = MIDIEvent_GetTime(pMIDIEvent);
			ltime_ms = MIDIData_TimeToMillisec(pMIDIData,ltime);
			MIDIEvent_GetTextA(pMIDIEvent, szBuf, 128);
			xstr = szBuf;
			ystr=lyric_util.strConv_2Utf(xstr, CP_KOREA);
			ystr = trim(ystr);
			_lyricEvent.tick = ltime;
			_lyricEvent.ms = ltime_ms;
			lyric_util.ReplaceAll(ystr, "\r", "@");//¶
			lyric_util.ReplaceAll(ystr, "\n", "@");
			lyric_util.ReplaceAll(ystr, " ", "_");
			_lyricEvent.utf8_str = ystr;
			_vLyricEvent.push_back(_lyricEvent);
			//vTime.push_back(ltime_ms);
		}
	}
	//put last time
	_lyricEvent.utf8_str = '^';
	_lyricEvent.tick = trackEndTime_tick;
	_lyricEvent.ms = MIDILyric_EndTime_ms;
	_vLyricEvent.push_back(_lyricEvent);
	return _vLyricEvent;
}

void MIDI_Util::makeHaWFILE(char* ofile, vector<HA_LYRIC> vHaLyric)
{
	FILE *pFile;
	HA_LYRIC haLyric;
	int i,id=1200;
	wchar_t version[8]={'h','c','s','k','1','0','0','0'};
	char dummy16[16]={0,};
	//printf("fopen ofile:%s\n",ofile);
	pFile=fopen(ofile, "wb"); 
	if(pFile == NULL){
		printf("fopen Error\n");
		return;
	}
	//printf("####makeHaWFILE vHaLyric.size[%d]\n",vHaLyric.size());
	fwrite(&id, 1, 2, pFile);//id
	fwrite(version, 1, 16, pFile);//version
	int cnt=vHaLyric.size();
	fwrite(&cnt, 1, 2, pFile);//카운트
	for(i=0;i<cnt;i++){
		wchar_t lyric = vHaLyric[i].uniStr;
		fwrite(&lyric, 1, 2, pFile);//가사
	}
	fwrite(dummy16, 1, 16, pFile);//dumy16
	for(i=0;i<cnt;i++){
		DWORD ltime = vHaLyric[i].ms;
		fwrite(&ltime, 1, 4, pFile);//가사
	}
	fclose(pFile);

}

int MIDI_Util::makeHaMidiW(char* txtfile, char* dst_file){

	vector<wchar_t> vUniText;
	vector<HA_LYRIC> vHaLyric;
	vector<HA_LYRIC> vFinalLyric;
	vector<string> vGasaString;
	vector<LYRIC_EVENT> vmLEvt;
	HA_LYRIC _ha_lyric;
	HA_LYRIC mLyric;

	char midfile[256];
	char buf[256];
	char utf_buf[256];
	char* ch_buf;
	wchar_t *uni_buf;
	wchar_t* w_buf;
	wchar_t municode[128];
	wchar_t uniChar;
	DWORD ltime_ms;
	int i, ix, iy, iz;
	string str,xstr, tstr, mstr;
	str= txtfile;
	mstr=lyric_util.ReplaceAll(str,".txt",".mid");
	sprintf(midfile,"%s",mstr.c_str());

	vmLEvt =HaMidiLyricEvent(midfile);
	uni_buf = lyric_util.getUNIBufFromTXT(txtfile);

	if(vmLEvt.size()==0) return 0;

	for (i = 0; i < vmLEvt.size(); i++){
		LYRIC_EVENT le = vmLEvt[i];
		xstr = le.utf8_str;
		if (xstr.find("^") != string::npos) {
			vLyricEvent.push_back(le);
			break;
		}
		else if (xstr.find("@") != string::npos) {
			int pre_pos = vLyricEvent.size() - 1;
			string pre_str = vLyricEvent[pre_pos].utf8_str;
			vLyricEvent[pre_pos].utf8_str = pre_str + "@";
		}
		else {
			vLyricEvent.push_back(le);
		}
	}

	iz = 0;
	for (i = 0; i < vLyricEvent.size(); i++) {
		LYRIC_EVENT le = vLyricEvent[i];
		str = le.utf8_str;
		sprintf(utf_buf, "%s", str.c_str());
		w_buf = lyric_util.utf2uni(utf_buf);
		//printf("(1)[%s] i[%04d] iz[%04d] -w [%04x][%04x][%04x][%04x]-[%04x][%04x][%04x][%04x]\n", le.utf8_str.c_str(), i, iz, w_buf[0], w_buf[1], w_buf[2], w_buf[3], w_buf[4], w_buf[5], w_buf[6], w_buf[7]);
		//printf("(1)[%s] i[%04d] iz[%04d] -z [%04x][%04x][%04x][%04x]-[%04x][%04x][%04x][%04x]\n", le.utf8_str.c_str(), i, iz, uni_buf[iz], uni_buf[iz + 1], uni_buf[iz + 2], uni_buf[iz + 3], uni_buf[iz+4], uni_buf[iz +51], uni_buf[iz +6], uni_buf[iz + 7]);
		//printf("(1)iz[%04d] - [%04x][%04x][%04x][%04x]\n", iz, uni_buf[iz], uni_buf[iz+1], uni_buf[iz+2], uni_buf[iz+3]);
		for (ix = 0; ix < 128; ix++) {
			//printf("(1-1) le[%04d] ix[%04d] iz[%04d] uni_buf[%04x][%04x]-w_buf[%04x][%04x]\n", i,ix, iz, uni_buf[iz], uni_buf[iz+1], w_buf[ix], w_buf[ix+1]);
			if (w_buf[ix] == 0) {
				//printf("(1-1) stop w[%04x] uni_buf[%04x]\n", w_buf[ix], uni_buf[iz]);
				//printf("(1-2) le[%04d][%04d] uni_buf[%04x]-[%04x][%04x][%04x][%04x]\n", i, iz, uni_buf[iz-1], w_buf[0], w_buf[1], w_buf[2], w_buf[3]);
				if (uni_buf[iz] == 0x5F) { 
					w_buf[ix] = 0x5F; 
					iz++; 
				}
				break;
			}
			if (w_buf[ix] == uni_buf[iz++]) continue;
		}

		memcpy(utf_buf, lyric_util.uni2utf(w_buf), 128);
		ch_buf = NULL;
		ch_buf = lyric_util.uni2utf(w_buf);
		str = ch_buf;
		vLyricEvent[i].utf8_str=str;
		//printf("(2)[%s] [%04d][%04d] -[%04x][%04x][%04x][%04x]-[%04x][%04x][%04x][%04x]\n",	str.c_str(), i, iz, w_buf[0], w_buf[1], w_buf[2], w_buf[3], w_buf[4], w_buf[5], w_buf[6], w_buf[7]);
		//printf("[%s]\n", str.c_str());
	}

#ifdef TXT_DBG	
	for (i = 0; i < 400; i++) {
		uniChar = uni_buf[i];
		printf("uni_buf[%04d] [%04x]\n", i,uni_buf[i]);
	}
#endif

#ifdef TXT_DBG
	xstr = "";
	for (i = 0; i < vLyricEvent.size(); i++) {
		xstr = xstr + vLyricEvent[i].utf8_str;
		//printf("vLyricEvent[%04d] [%s]\n", i, vLyricEvent[i].utf8_str.c_str());
	}
	lyric_util.ReplaceAll(xstr, "@", "\n");
	printf("\n%s\n", xstr.c_str());
#endif

#if 0
	//adjust Enter(@) time
	for (i = 0; i < vLyricEvent.size(); i++) {
		xstr = vLyricEvent[i].utf8_str;
		if (xstr.find("@") != string::npos) {
			if ((vLyricEvent[i + 1].ms - vLyricEvent[i].ms) > 600) {
				vLyricEvent[i].ms = vLyricEvent[i + 1].ms-600;
			}
			else vLyricEvent[i].ms = vLyricEvent[i + 1].ms;
		}
	}
#endif
	//make duration
	for (i = 0; i < vLyricEvent.size()-1; i++) {
		long duration;
		duration = vLyricEvent[i+1].ms- vLyricEvent[i].ms;
		vLyricEvent[i].duration = duration;
	}
	vLyricEvent[vLyricEvent.size() - 1].duration = 200;

	//for (i = 0; i < vLyricEvent.size(); i++) {
	//	printf("vLyricEvent[%04d] duration[%04d]\n", i, vLyricEvent[i].duration);
	//}

	///make vHaLyric
	//모든 글자별로 이벤트만든다
	vHaLyric.clear();
	int cnt,delta;
	for (i = 0; i < vLyricEvent.size(); i++) {
		//printf("Event[%04d] ltime[%04d] ms[%04d] lyric[%s]\n", i, vLyricEvent[i].tick, vLyricEvent[i].ms, vLyricEvent[i].utf8_str.c_str());
		xstr = vLyricEvent[i].utf8_str;
		ltime_ms= vLyricEvent[i].ms;
		sprintf(buf, "%s", xstr.c_str());
		//각 글자의 Duration 전체 Duration/글자수
		uni_buf = lyric_util.utf2uni(buf);
		cnt = 0;
		for (ix = 0; ix < 256; ix++) {
			uniChar = uni_buf[ix];
			if (uniChar == 0) break;
			else if (uniChar == '@' || uniChar == '_' || uniChar == '^') { ; }
			else cnt++;
		}
		if (cnt < 2)delta = 0;//글자수가 한개인 경우 Delta값은 "0"이다
		else delta= vLyricEvent[i].duration/(cnt);
		//if(cnt>2)
		//	printf("vHaLyric Event[%04d] cnt[%04d] ms[%04d] delta[%d] duration[%04d] lyric[%s]\n", i, cnt, ltime_ms, delta, vLyricEvent[i].duration, xstr.c_str());
		for(ix=0;ix<128;ix++) {
			uniChar = uni_buf[ix];
			if (uniChar == 0) break;
			else {
				if(uniChar == '@' || uniChar == '_'|| ix==0) ltime_ms = ltime_ms ;
				else ltime_ms = ltime_ms + delta;
				_ha_lyric.ms = ltime_ms;
				_ha_lyric.uniStr = uniChar;
				//if (cnt > 2) {
				//	printf("ix[%04d] [%x][%d]\n", ix, uniChar, ltime_ms);
				//}
				vHaLyric.push_back(_ha_lyric);
			}
		}
	}

	//for (i = 0; i < vHaLyric.size(); i++) {
	//	printf("Event[%04d] [%04x] [%04d]\n", i, vHaLyric[i].uniStr, vHaLyric[i].ms);
	//}
	//printf("MIDILyric_StartTime_ms[%04d]\n", MIDILyric_StartTime_ms);
	//printf("MIDILyric_EndTime_ms[%04d]\n", MIDILyric_EndTime_ms);

	vFinalLyric.clear();
	mLyric = vHaLyric[0];
	DWORD mltime = mLyric.ms;
	if (mltime > 4000)
	{
			mLyric.ms = 0;		mLyric.uniStr = 0x01; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3600;	mLyric.uniStr = 0x09; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3500;	mLyric.uniStr = 0x07; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3400;	mLyric.uniStr = 0x06; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3300;	mLyric.uniStr = 0x05; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3200;	mLyric.uniStr = 0x04; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime - 3000;	mLyric.uniStr = 0x03; vFinalLyric.push_back(mLyric);
			mLyric.ms = mltime;		mLyric.uniStr = 0x0A; vFinalLyric.push_back(mLyric);
	}
	else if (mltime > 2000)
		//if(MIDILyric_StartTime_ms>9)
	{
		mLyric.ms = 0;		mLyric.uniStr = 0x01; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1600;	mLyric.uniStr = 0x09; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1500;	mLyric.uniStr = 0x07; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1400;	mLyric.uniStr = 0x06; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1300;	mLyric.uniStr = 0x05; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1200;	mLyric.uniStr = 0x04; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime - 1000;	mLyric.uniStr = 0x03; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;		mLyric.uniStr = 0x0A; vFinalLyric.push_back(mLyric);
	}
	else 
	{
		mLyric.ms = 0;						mLyric.uniStr = 0x01; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x09; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x07; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x06; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x05; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x04; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x03; vFinalLyric.push_back(mLyric);
		mLyric.ms = mltime;	mLyric.uniStr = 0x0A; vFinalLyric.push_back(mLyric);
	}

//"*":0x2A 0x01
//"/":0x2f 0x0a
//"_":0x5f 0x0c
//"@":0x40 0x0b
	for (i = 0; i < vHaLyric.size(); i++) {
		//printf("vHaLyric Event[%04d] ms[%04d] lyric[%04x]\n", i, vHaLyric[i].tick, vHaLyric[i].uniStr);
		mLyric = vHaLyric[i];
		if (mLyric.uniStr == '^') { 
			mLyric.uniStr = 0x0b; vFinalLyric.push_back(mLyric);
		}
		else if (mLyric.uniStr == '_') {
			mLyric.uniStr = 0x0C; vFinalLyric.push_back(mLyric);
		}
		else if (mLyric.uniStr == '@') {
				mLyric.uniStr = 0x0A; vFinalLyric.push_back(mLyric);
		}
		else {
			vFinalLyric.push_back(mLyric);
		}
	}
	for (i = 0; i < vFinalLyric.size(); i++) {
		if (vFinalLyric[i].uniStr == 0x0c || vFinalLyric[i].uniStr == 0x0A) {
			vFinalLyric[i].ms = vFinalLyric[i + 1].ms;
		}
	}
	for (i = 0; i < vFinalLyric.size(); i++) {
		mLyric = vFinalLyric[i];
#ifdef TXT_DBG
		printf("vFinalLyric[%04d] ms[%04d] lyric[%04x]\n", i, mLyric.ms, mLyric.uniStr);
#endif
	}
	makeHaWFILE(dst_file, vFinalLyric);
	return 1;
}


